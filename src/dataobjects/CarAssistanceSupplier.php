<?php

namespace Hestec\Product;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Control\Director;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\RequiredFields;

class CarAssistanceSupplier extends DataObject {

    private static $singular_name = 'CarAssistanceSupplier';
    private static $plural_name = 'CarAssistancesSuppliers';

    private static $table_name = 'ProductCarAssistanceSupplier';

    private static $db = array(
        'Name' => 'Varchar(255)'
    );

    private static $has_one = array(
        'CarAssistanceAdmin' => CarAssistanceAdmin::class
    );

    private static $has_many = array(
        'CarAssistances' => CarAssistance::class
    );

    /*private static $many_many = array(
        'Categories' => Category::class
    );*/

    private static $summary_fields = array(
        'Name'
    );

    /*public function getCMSFields() {

        $NameField = TextField::create('Name', 'Name');

        return new FieldList(
            $NameField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'Name',
        ));

    }*/

}