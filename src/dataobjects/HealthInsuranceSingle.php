<?php

namespace Hestec\Product;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Control\Director;
use SilverStripe\ORM\FieldType\DBField;
use Hestec\LinkManager\Link;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\OptionsetField;
use Symbiote\MultiValueField\Fields\MultiValueTextField;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\NumericField;


class HealthInsuranceSingle extends DataObject {

    private static $singular_name = 'HealthInsuranceSingle';
    private static $plural_name = 'HealthInsuranceSingles';

    private static $table_name = 'ProductHealthInsuranceSingle';

    private static $db = array(
        'Year' => 'Int',
        'Type' => "Enum('RESTITUTION,NATURA,BUDGET,COMBINATION','')",
        'Name' => 'Varchar(255)',
        'Price' => 'Currency',
        'PriceMaxRisk' => 'Currency'
    );

    private static $has_one = array(
        'HealthInsuranceSupplier' => HealthInsuranceSupplier::class,
    );

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return "€ ".$output;

    }

    public function InsuranceName()
    {

        if (strlen($this->Name) < 3){

            switch ($this->Type) {
                case "RESTITUTION":
                    $name = "Restitutiepolis";
                    break;
                case "NATURA":
                    $name = "Naturapolis";
                    break;
                case "BUDGET":
                    $name = "Budgetpolis";
                    break;
                case "COMBINATION":
                    $name = "Combinatiepolis";
                    break;
                default:
                    $name = "Zorgpolis";
                    break;
            }
            return $name;
        }
        return $this->Name;

    }


}