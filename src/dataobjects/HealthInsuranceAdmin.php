<?php

namespace Hestec\Product;

use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Security\Permission;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class HealthInsuranceAdmin extends DataObject {

    private static $singular_name = 'HealthInsuranceAdmin';
    private static $plural_name = 'HealthInsurancesAdmins';

    private static $table_name = 'ProductHealthInsuranceAdmin';

    private static $db = array(
        'CurrentYear' => 'Int',
    );

    private static $has_many = array(
        'HealthInsuranceSuppliers' => HealthInsuranceSupplier::class
    );

    /*private static $many_many = array(
        'Categories' => Category::class
    );*/

    private static $summary_fields = array(
        'getRecordTitle'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['getRecordTitle'] = '';

        return $labels;
    }

    public function getRecordTitle() {

        return DBField::create_field('Text', "Click to open");

    }

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (HealthInsuranceAdmin::get()->count() == 0){

            $record = new HealthInsuranceAdmin();
            $record->write();

        }

    }

    public function getCMSFields() {

        $fields = FieldList::create(TabSet::create('Root'));

        $HealthInsuranceSuppliersGridField = GridField::create(
            'HealthInsuranceSuppliers',
            'HealthInsuranceSuppliers',
            $this->HealthInsuranceSuppliers(),
            GridFieldConfig_RecordEditor::create()
                //->addComponent(new GridFieldOrderableRows())
        );

        /*$HealthInsurancesGridField = GridField::create(
            'HealthInsurances',
            'HealthInsurances',
            $this->HealthInsurances(),
            GridFieldConfig_RecordEditor::create()
                //->addComponent(new GridFieldOrderableRows())
        );*/

        $currentdate = new \DateTime();
        $year = $currentdate->format('Y');

        $CurrentYearSource = array(
            $year => $year,
            $year+1 => $year+1
        );

        $CurrentYearField = DropdownField::create('CurrentYear', "CurrentYear", $CurrentYearSource);
        $CurrentYearField->setEmptyString("(Select)");
        $CurrentYearField->setDescription("The offers of which year must be displayed? (Usually from half November the next year.)");

        $fields->addFieldsToTab('Root.Main', array(
            $CurrentYearField
        ));

        $fields->addFieldsToTab('Root.Suppliers', array(
            $HealthInsuranceSuppliersGridField
        ));

        /*$fields->addFieldsToTab('Root.HealthInsurances', array(
            $HealthInsurancesGridField
        ));*/

        return $fields;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return false;
    }

    public function canCreate($member = null, $context = [])
    {
        return false;
    }

}