<?php

namespace Hestec\Product;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Control\Director;
use SilverStripe\ORM\FieldType\DBField;
use Hestec\LinkManager\Link;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;

class CarInsurance extends DataObject {

    private static $singular_name = 'CarInsurance';
    private static $plural_name = 'CarInsurances';

    private static $table_name = 'ProductCarInsurance';

    private static $db = array(
        'Title' => 'Varchar(255)',
        'Description' => 'Text',
        'Category' => "Enum('WA,WAPLUS,ALLRISK',''",
        'PriceMonthly' => 'Currency',
        'PriceAnnually' => 'Currency',
        'PriceIntial' => 'Currency',
        'OwnRisk' => 'Currency',
        'Enabled' => 'Boolean',
        'InternalNotes' => 'Text',
        'Sort' => 'Int'
    );

    private static $has_one = array(
        'CarInsuranceAdmin' => CarInsuranceAdmin::class,
        'CarInsuranceSupplier' => CarInsuranceSupplier::class,
        'AffiliateLink' => Link::class
    );

    /*private static $many_many = array(
        'Categories' => Category::class
    );*/

    private static $summary_fields = array(
        'CarInsuranceSupplier.Name',
        'Title',
        'Category',
        'PriceMonthly',
        'PriceAnnually',
        'PriceIntial',
        'OwnRisk',
        'Enabled.Nice'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['CarInsuranceSupplier.Name'] = "Supplier";
        $labels['PriceMonthly'] = "Monthly";
        $labels['PriceAnnually'] = "Annually";
        $labels['PriceIntial'] = "Intial";
        $labels['Enabled.Nice'] = "Enabled";

        return $labels;
    }

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return "€ ".$output;

    }

    public function PricePerYear(){

        if ($this->PriceAnnually > 0) {
            return $this->PriceAnnually;
        }
        return $this->PriceMonthly * 12;

    }

}