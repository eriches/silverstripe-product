<?php

namespace Hestec\Product;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Security\Permission;
use SilverStripe\Forms\GridField\GridField;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\DateField;

class CarAssistanceAdmin extends DataObject {

    private static $singular_name = 'CarAssistanceAdmin';
    private static $plural_name = 'CarAssistancesAdmins';

    private static $table_name = 'ProductCarAssistanceAdmin';

    private static $db = array(
        'Disclaimer' => 'HTMLText',
        'Explanation' => 'HTMLText',
        'LastUpdate' => 'Date',
        'LastUpdateAddons' => 'Date'
    );

    private static $has_many = array(
        'CarAssistances' => CarAssistance::class,
        'CarAssistanceSuppliers' => CarAssistanceSupplier::class
    );

    /*private static $many_many = array(
        'Categories' => Category::class
    );*/

    private static $summary_fields = array(
        'getRecordTitle'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['getRecordTitle'] = '';

        return $labels;
    }

    public function getRecordTitle() {

        return DBField::create_field('Text', "Click to open");

    }

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (CarAssistanceAdmin::get()->count() == 0){

            $record = new CarAssistanceAdmin();
            $record->write();

        }

    }

    public function getCMSFields() {

        $fields = FieldList::create(TabSet::create('Root'));

        $CarAssistanceSuppliersGridField = GridField::create(
            'CarAssistanceSuppliers',
            'CarAssistanceSuppliers',
            $this->CarAssistanceSuppliers(),
            GridFieldConfig_RecordEditor::create()
                //->addComponent(new GridFieldOrderableRows())
        );

        $CarAssistancesGridField = GridField::create(
            'CarAssistances',
            'CarAssistances',
            $this->CarAssistances(),
            GridFieldConfig_RecordEditor::create()
                ->addComponent(new GridFieldOrderableRows())
        );

        $DisclaimerField = HTMLEditorField::create('Disclaimer', "Disclaimer");
        $DisclaimerField->setRows(15);
        $ExplanationField = HTMLEditorField::create('Explanation', "Explanation");
        $ExplanationField->setRows(15);
        $LastUpdateField = DateField::create('LastUpdate', "LastUpdate");
        $LastUpdateAddonsField = DateField::create('LastUpdateAddons', "LastUpdateAddons");


        $fields->addFieldsToTab('Root.Main', array(
            $DisclaimerField,
            $ExplanationField,
            $LastUpdateField,
            $LastUpdateAddonsField
        ));

        $fields->addFieldsToTab('Root.Suppliers', array(
            $CarAssistanceSuppliersGridField
        ));

        $fields->addFieldsToTab('Root.CarAssistances', array(
            $CarAssistancesGridField
        ));

        return $fields;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return false;
    }

    public function canCreate($member = null, $context = [])
    {
        return false;
    }

}