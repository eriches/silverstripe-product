<?php

namespace Hestec\Product;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Security\Permission;
use SilverStripe\Forms\GridField\GridField;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;


class CarInsuranceAdmin extends DataObject {

    private static $singular_name = 'CarInsuranceAdmin';
    private static $plural_name = 'CarInsurancesAdmins';

    private static $table_name = 'ProductCarInsuranceAdmin';

    private static $db = array(
        'Disclaimer' => 'HTMLText',
        'Explanation' => 'HTMLText'
    );

    private static $has_many = array(
        'CarInsurances' => CarInsurance::class,
        'CarInsuranceSuppliers' => CarInsuranceSupplier::class
    );

    /*private static $many_many = array(
        'Categories' => Category::class
    );*/

    private static $summary_fields = array(
        'getRecordTitle'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['getRecordTitle'] = '';

        return $labels;
    }

    public function getRecordTitle() {

        return DBField::create_field('Text', "Click to open");

    }

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (CarInsuranceAdmin::get()->count() == 0){

            $record = new CarInsuranceAdmin();
            $record->write();

        }

    }

    public function getCMSFields() {

        $fields = FieldList::create(TabSet::create('Root'));

        $CarInsuranceSuppliersGridField = GridField::create(
            'CarInsuranceSuppliers',
            'CarInsuranceSuppliers',
            $this->CarInsuranceSuppliers(),
            GridFieldConfig_RecordEditor::create()
                //->addComponent(new GridFieldOrderableRows())
        );

        $CarInsurancesGridField = GridField::create(
            'CarInsurances',
            'CarInsurances',
            $this->CarInsurances(),
            GridFieldConfig_RecordEditor::create()
                ->addComponent(new GridFieldOrderableRows())
        );

        $DisclaimerField = HTMLEditorField::create('Disclaimer', "Disclaimer");
        $DisclaimerField->setRows(15);
        $ExplanationField = HTMLEditorField::create('Explanation', "Explanation");
        $ExplanationField->setRows(15);

        $fields->addFieldsToTab('Root.Main', array(
            $DisclaimerField,
            $ExplanationField
        ));

        $fields->addFieldsToTab('Root.Suppliers', array(
            $CarInsuranceSuppliersGridField
        ));

        $fields->addFieldsToTab('Root.CarInsurances', array(
            $CarInsurancesGridField
        ));

        return $fields;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return false;
    }

    public function canCreate($member = null, $context = [])
    {
        return false;
    }

}