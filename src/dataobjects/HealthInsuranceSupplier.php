<?php

namespace Hestec\Product;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use Unisolutions\GridField\CopyButton;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class HealthInsuranceSupplier extends DataObject {

    private static $singular_name = 'HealthInsuranceSupplier';
    private static $plural_name = 'HealthInsuranceSuppliers';

    private static $table_name = 'ProductHealthInsuranceSupplier';

    private static $db = array(
        'Enabled' => 'Boolean',
        'Name' => 'Varchar(255)'
    );

    private static $has_one = array(
        'HealthInsuranceAdmin' => HealthInsuranceAdmin::class
    );

    private static $has_many = array(
        'HealthInsurances' => HealthInsurance::class
    );

    private static $summary_fields = array(
        'Enabled.Nice',
        'Name'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['Enabled.Nice'] = "Enabled";

        return $labels;
    }

    public function getCMSFields() {

        $EnabledField = CheckboxField::create('Enabled', "Enabled");
        $NameField = TextField::create('Name', 'Name');

        $HealthInsurancesGridField = GridField::create(
            'HealthInsurances',
            "HealthInsurances",
            $this->HealthInsurances(),
            GridFieldConfig_RecordEditor::create()
                //->addComponent(new GridFieldOrderableRows())
                ->addComponent(new CopyButton(), 'GridFieldEditButton')
        );


        $fields = new FieldList(
            $EnabledField,
            $NameField,
            $HealthInsurancesGridField
        );

        $this->extend('updateCMSFields', $fields);

        return $fields;

    }

    /*public function getCMSValidator() {

        return new RequiredFields(array(
            'Name',
        ));

    }*/

    public function getPrice($year) {

        if ($output = $this->HealthInsurances()->filter(array('Year' => $year))->first()){

            return $output;

        }
        return false;

    }

    public function CurrentYear() {

        return $this->HealthInsuranceAdmin()->CurrentYear;

    }

    public function LastYear() {

        return $this->HealthInsuranceAdmin()->CurrentYear-1;

    }

    public function PriceEuro($price){

        if (is_numeric($price) && $price > 0){
            $output = number_format($price, 2, ',', '');

            return "€ ".$output;
        }
        return "-";


    }

}