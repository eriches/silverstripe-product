<?php

namespace Hestec\Product;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Control\Director;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Assets\Image;

class CarInsuranceSupplier extends DataObject {

    private static $singular_name = 'CarInsuranceSupplier';
    private static $plural_name = 'CarInsurancesSuppliers';

    private static $table_name = 'ProductCarInsuranceSupplier';

    private static $db = array(
        'Name' => 'Varchar(255)'
    );

    private static $has_one = array(
        'CarInsuranceAdmin' => CarInsuranceAdmin::class
    );

    private static $has_many = array(
        'CarInsurances' => CarInsurance::class
    );

    /*private static $many_many = array(
        'Categories' => Category::class
    );*/

    private static $summary_fields = array(
        'Name'
    );

}