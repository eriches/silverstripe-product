<?php

namespace Hestec\Product;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Control\Director;
use SilverStripe\ORM\FieldType\DBField;
use Hestec\LinkManager\Link;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\OptionsetField;
use Symbiote\MultiValueField\Fields\MultiValueTextField;
use SilverStripe\Forms\TabSet;


class CarAssistance extends DataObject {

    private static $singular_name = 'CarAssistance';
    private static $plural_name = 'CarAssistances';

    private static $table_name = 'ProductCarAssistance';

    private static $db = array(
        'Title' => 'Varchar(255)',
        'Description' => 'Text',
        'OwnCountry' => 'Boolean',
        'Residence' => 'Boolean',
        'Europe' => 'Boolean',
        'ReplacementCar' => 'Boolean',
        'LegalAssistance' => 'Boolean',
        'Hotel' => 'Boolean',
        'Repatriation' => 'Boolean',
        'Consumer' => 'Boolean',
        'Business' => 'Boolean',
        'AnwbMember' => 'Boolean',
        'CarInsuranceAddOn' => 'Boolean',
        'RegistrationType' => "Enum('CAR,PERSON','')",
        'PriceAnnually' => 'Currency',
        'OfferText' => 'Varchar(255)',
        'OfferStartDate' => 'Date',
        'OfferEndDate' => 'Date',
        'Offer2Text' => 'Varchar(255)',
        'Offer2StartDate' => 'Date',
        'Offer2EndDate' => 'Date',
        'PermanentOffer' => 'Varchar(255)',
        'Date' => 'Date',
        'ExtraInfo'	=> 'MultiValueField',
        'ToolTip'	=> 'Varchar(255)',
        'Enabled' => 'Boolean',
        'InternalNotes' => 'Text',
        'Sort' => 'Int'
    );

    private static $has_one = array(
        'CarAssistanceAdmin' => CarAssistanceAdmin::class,
        'CarAssistanceSupplier' => CarAssistanceSupplier::class,
        'AffiliateLink' => Link::class
    );

    /*private static $many_many = array(
        'Categories' => Category::class
    );*/

    private static $summary_fields = array(
        'CarAssistanceSupplier.Name',
        'CarInsuranceAddOn.Nice',
        'Title',
        'OwnCountry.Nice',
        'Residence.Nice',
        'Europe.Nice',
        'ReplacementCar.Nice',
        'PriceAnnually',
        'Enabled.Nice'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['CarAssistanceSupplier.Name'] = "Supplier";
        $labels['CarInsuranceAddOn.Nice'] = "Addon";
        $labels['OwnCountry.Nice'] = "National";
        $labels['Residence.Nice'] = "Residence";
        $labels['Europe.Nice'] = "Europe";
        $labels['ReplacementCar.Nice'] = "ReplacementCar";
        $labels['PriceAnnually'] = "Annually";
        $labels['Enabled.Nice'] = "Enabled";

        return $labels;
    }

    public function getCMSFields() {

        $fields = FieldList::create(TabSet::create('Root'));

        $EnabledField = CheckboxField::create('Enabled', "Enabled");
        $TitleField = TextField::create('Title', 'Title');
        $DescriptionField = TextareaField::create('Description', 'Description');
        $OwnCountryField = CheckboxField::create('OwnCountry', "National");
        $ResidenceField = CheckboxField::create('Residence', "Residence");
        $EuropeField = CheckboxField::create('Europe', "Europe");
        $AnwbMemberField = CheckboxField::create('AnwbMember', "AnwbMember");
        $ReplacementCarField = CheckboxField::create('ReplacementCar', "ReplacementCar");
        $LegalAssistanceField = CheckboxField::create('LegalAssistance', "LegalAssistance");
        $HotelField = CheckboxField::create('Hotel', "Hotel");
        $RepatriationField = CheckboxField::create('Repatriation', "Repatriation");
        $ConsumerField = CheckboxField::create('Consumer', "Consumer");
        $BusinessField = CheckboxField::create('Business', "Business");
        $CarInsuranceAddOnField = CheckboxField::create('CarInsuranceAddOn', "CarInsuranceAddOn");
        $PriceAnnuallyField = CurrencyField::create('PriceAnnually', "PriceAnnually");
        $OfferTextField = TextField::create('OfferText', "OfferText");
        $OfferStartDateField = DateField::create('OfferStartDate', "OfferStartDate");
        $OfferEndDateField = DateField::create('OfferEndDate', "OfferEndDate");
        $Offer2TextField = TextField::create('Offer2Text', "Offer2Text");
        $Offer2StartDateField = DateField::create('Offer2StartDate', "Offer2StartDate");
        $Offer2EndDateField = DateField::create('Offer2EndDate', "Offer2EndDate");
        $PermanentOfferField = TextField::create('PermanentOffer', "PermanentOffer");
        $ToolTipField = TextField::create('ToolTip', "ToolTip");
        $ExtraInfoField = MultiValueTextField::create('ExtraInfo', "ExtraInfo");

        $RegistrationTypeField = OptionsetField::create('RegistrationType', "RegistrationType", $this->dbObject('RegistrationType')->enumValues());

        $CarAssistanceSupplierSource = CarAssistanceSupplier::get();

        $CarAssistanceSupplierField = DropdownField::create('CarAssistanceSupplierID', "CarAssistanceSupplier", $CarAssistanceSupplierSource);

        $LinkSource = Link::get()->map('ID', 'TitleInternTitle');

        $AffiliateLinkField = DropdownField::create('AffiliateLinkID', "AffiliateLink", $LinkSource);
        $AffiliateLinkField->setEmptyString("(select)");

        $InternalNotesField = TextareaField::create('InternalNotes', "InternalNotes");

        $fields->addFieldsToTab('Root.Main', array(
            $EnabledField,
            $CarAssistanceSupplierField,
            $CarInsuranceAddOnField,
            $TitleField,
            $DescriptionField,
            $OwnCountryField,
            $ResidenceField,
            $EuropeField,
            $ReplacementCarField,
            $LegalAssistanceField,
            $HotelField,
            $RepatriationField,
            $ConsumerField,
            $BusinessField,
            $AnwbMemberField,
            $RegistrationTypeField,
            $PriceAnnuallyField,
            $OfferTextField,
            $OfferStartDateField,
            $OfferEndDateField,
            $Offer2TextField,
            $Offer2StartDateField,
            $Offer2EndDateField,
            $PermanentOfferField,
            $ExtraInfoField,
            $ToolTipField,
            $AffiliateLinkField
        ));

        $fields->addFieldsToTab('Root.Internal', array(
            $InternalNotesField
        ));

        return $fields;

    }

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return "€ ".$output;

    }

    public function PricePerMonth(){

        if ($this->PriceMonthly > 0) {
            return $this->PriceMonthly;
        }
        return $this->PriceAnnually / 12;

    }

    public function OfferActive(){

        $currentdate = new \DateTime(date('Y-m-d'));
        $startdate = new \DateTime($this->OfferStartDate);
        $enddate = new \DateTime($this->OfferEndDate);

        if (strlen($this->OfferText) > 3 && $startdate <= $currentdate && $enddate >= $currentdate){

            return true;

        }
        return false;

    }

    public function Offer2Active(){

        $currentdate = new \DateTime(date('Y-m-d'));
        $startdate = new \DateTime($this->Offer2StartDate);
        $enddate = new \DateTime($this->Offer2EndDate);

        if (strlen($this->Offer2Text) > 3 && $startdate <= $currentdate && $enddate >= $currentdate){

            return true;

        }
        return false;

    }

}